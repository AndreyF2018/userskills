﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;
using UserSkills.Models.Entities;

namespace UserSkills.Controllers
{
    public class HomeController : Controller
    {
        SkillsController db = new SkillsController();
        SkillContext sc = new SkillContext();
        public ActionResult Index()
        {
           // return View(db.GetSkills());
            return View("SkillList", db.GetSkills());
        }

        [HttpGet]
        public ActionResult Delete(int ID)
        {
            return View("Delete");
            
        }

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int ID)
        {
            db.Delete(ID);
            return RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult Edit()
        {
            return View("Edit");
        }

        [HttpPost, ActionName("Edit")]
        public ActionResult Edit(Skill skill)
        {
            sc.Entry(skill).State = EntityState.Modified;
            sc.SaveChanges();    
            //skill = sc.Skills.Find(skill.skillID);        
            //Skill oldSkill = sc.Skills.Find(skill.skillID);
            //skill.title = oldSkill.title;
            //skill.description = oldSkill.description;
            //sc.Entry(skill).State = EntityState.Modified;
            return RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult Create()
        {
            
            return View("Create");
        }

        [HttpPost, ActionName("Create")]
        public ActionResult Create(Skill skill)
        {
            sc.Skills.Add(skill);
            sc.SaveChanges();
            return RedirectToAction("Index");

        }

        public ActionResult Details(Skill skill)
        {
            
            return View("Details", skill);
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}