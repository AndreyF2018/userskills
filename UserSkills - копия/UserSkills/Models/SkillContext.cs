﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;

namespace UserSkills.Models.Entities
{
    public class SkillContext : DbContext
    {
        public DbSet<Skill> Skills { get; set; }
    }
}