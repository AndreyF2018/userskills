﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UserSkills.Models.Entities
{
    public class Skill
    {
        public int skillID { get; set; }
        public string title { get; set; }
        public string description { get; set; }
    }
}