﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(UserSkills.Startup))]
namespace UserSkills
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
